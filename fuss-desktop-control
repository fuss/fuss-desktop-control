#!/usr/bin/env python
#
#
#  File: fuss-desktop-control
#
#  Copyright (C) 2010 Christopher R. Gabriel <cgabriel@truelite.it>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#

__netport = 13401

POPUP_DURATION_SEC = 30

import sys
import os
import os.path
import gconf
import ConfigParser

gconf_keys = {
    "GTK_THEME": "/desktop/gnome/interface/gtk_theme",
    "METACITY_THEME": "/apps/metacity/general/theme",
    "ICON_THEME": "/desktop/gnome/interface/icon_theme",
    "NOTIFICATION_THEME": "/apps/notification-daemon/theme",
    "COLOR_SCHEME": "/desktop/gnome/interface/gtk_color_scheme",
    "LOCKDOWN": "/desktop/gnome/lockdown/disable_theme_settings",
    "BACKGROUND": "/desktop/gnome/background/picture_filename",
    "APPLICATION_FONT": "/desktop/gnome/interface/font_name",
    "DOCUMENTS_FONT": "/desktop/gnome/interface/document_font_name",
    "DESKTOP_FONT": "/apps/nautilus/preferences/desktop_font",
    "WINDOWTITLE_FONT": "/apps/metacity/general/titlebar_font",
    "MONOSPACE_FONT": "/desktop/gnome/interface/monospace_font_name"
    }

try:
    from gettext import gettext as _
    from twisted.internet import gtk2reactor
    gtk2reactor.install()

    from twisted.application import internet, service
    from twisted.internet import protocol, defer, reactor
    from twisted.web import resource, server, static, xmlrpc

    from twisted.web import xmlrpc, resource, server

    import time
    import dbus
    import gtk, gobject

except ImportError, e:
    print "Error loading python libraries:"
    print e
    sys.exit(1)

if not os.environ.has_key("DISPLAY"):
    print "Could not opend display"
    sys.exit(2)


### GCONF STUFF


def set_gconf(key, value):
    gconf_client = gconf.client_get_default()
    m = None
    if isinstance(value, int):
        m = "set_int"
    if isinstance(value, float):
        m = "set_float"
    if isinstance(value, bool):
        m = "set_bool"
    if isinstance(value, list):
        m = "set_list"
    if isinstance(value, str) or isinstance(value, unicode):
        m = "set_string"
    if not m:
        return None
    method = getattr(gconf_client, m)
    try:
        return method(key, value)
    except:
        return None

## set theme by domain, see #483
def set_theme_by_domain():
    p = os.popen("hostname --fqdn")
    fqdn = p.read().strip()
    p.close()
    s = fqdn.split(".")
    if len(s)  < 2:
        return
    domain = s[1]
    # read config, discover mapping
    # between domains and themes,
    # then apply the theme.
    c = ConfigParser.ConfigParser()
    files = ['/etc/fuss-desktop-control.conf',
             'fuss-desktop-control.conf']
    file = None
    for f in files:
        if os.path.isfile(f):
            file = f
            break
    if not file:
        print "Conf file not found"
        return
    c.read(f)
    sections = [x.split()[1] for x in c.sections() if 'domain' in x]
    our_domain = None
    for s in sections:
        if domain in s:
            our_domain = 'domain %s' %s
            break

    if not our_domain:
        return

    print "Working on", our_domain
    options = c.items(our_domain)
    for o in options:
        if o[0].upper() in gconf_keys.keys():
            k = gconf_keys[o[0].upper()]
            v = o[1]
            print "SETTING", k, "TO", v
            set_gconf(k, v)

try:
    control_file = os.path.join(os.environ['HOME'], ".fdc-theme")
    if not os.path.isfile(control_file):
        print set_theme_by_domain()
        f = open(control_file, "w")
        f.close()

except Exception, e:
    print "Can't set theme by domain:", e

#### SUPPORT FOR OCTOFUSS CLIENT #####
def record_display_size():
    import math
    lines = os.popen("xrandr -q").readlines()
    final_line = None
    for line in lines:
        if "mm" in line:
            final_line  = line
            break
    items = final_line.split()
    w = int(items[-1].replace("mm",""))
    h = int(items[-3].replace("mm",""))
    diagonal = int((math.sqrt((w**2)+(h**2))/10)/2.54)
    return diagonal

try:
    f = open("/var/tmp/fuss-display-size","w")
    diag = record_display_size()
    f.write('%s"' % diag)
    f.close()
except:
    pass

#######################################

# notification
sessionbus = dbus.SessionBus()
notify_obj = sessionbus.get_object('org.freedesktop.Notifications',
                                  '/org/freedesktop/Notifications')
notify_if = dbus.Interface(notify_obj, 'org.freedesktop.Notifications')

class BlockWindow(gtk.Window):
    def __init__(self, message="The network administrator request your attention"):
        gtk.Window.__init__(self)
        self.fullscreen()
        box = gtk.VBox()
        self.add(box)
        image = gtk.Image()
        IMAGE_FILE = "/usr/share/pixmaps/fuss-desktop-control.png"
        if not os.path.isfile(IMAGE_FILE):
            IMAGE_FILE = 'fuss-desktop-control.png'
        pixbuf = gtk.gdk.pixbuf_new_from_file(IMAGE_FILE)
        image.set_from_pixbuf(pixbuf)
        box.pack_start(image)
        self.message = message
        self.message_label = gtk.Label()
        box.pack_start(self.message_label)
        self.set_text(self.message)
        self.screen_is_locked = False

    def set_text(self, message):
        self.message = message
        self.message_label.set_text("<span size='xx-large'>"+message+"</span>")
        self.message_label.set_use_markup(True)

def lock_window(window):
    gtk.gdk.keyboard_grab(window, 1, 0)

class FussDesktopControl(xmlrpc.XMLRPC):
    def __init__(self):
        xmlrpc.XMLRPC.__init__(self)
        self.block_window = BlockWindow()

    def xmlrpc_screen_lock(self, message="The network administrator request your attention"):
        try:
            self.block_window.set_text(message)
            self.block_window.show_all()
            self.block_window.screen_is_locked = True
            t = gobject.timeout_add(10, lock_window, self.block_window.window)
            return True
        except:
            return False

    def xmlrpc_set_gconf(self, key, value):
        return set_gconf(key, value)

    def xmlrpc_screen_release(self):
        try:
            self.block_window.hide()
            self.block_window.screen_is_locked = False
            return True
        except:
            return False

    def xmlrpc_screen_is_locked(self):
        return self.block_window.screen_is_locked

    def xmlrpc_machine_status(self):
        status = []
        status.append(self.xmlrpc_logged_user())
        status.append(self.xmlrpc_screen_is_locked())
	return status

    def xmlrpc_popup_message(self,message="Message from network"):
        try:
            notify_if.Notify(_('Message from network'), 0, '', 'Message from network', message, [], {}, POPUP_DURATION_SEC*100000)
            return True
        except:
            return False

    def xmlrpc_logged_user(self):
        user = None
        if os.environ.has_key('LOGNAME'):
            user = os.environ['LOGNAME']
        return user

app = FussDesktopControl()

try:
	reactor.listenTCP(__netport, server.Site(app))
	reactor.run()
except:
	pass
